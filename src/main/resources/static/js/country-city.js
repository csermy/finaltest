$(function(){

	/*
		TODO:
		az oldal betöltésekor lekérni a /rest/countries URL-ről az országok listáját,
		és a város hozzáadása popup <select> elemébe berakni az <option>-öket
	*/
	
	$.get('/rest/countries', function(response){
		$.each(response, function(index, value){
			var $option = $('<option></option>').text(value.iso);
			$('#city-country').append($option);
		})
	});

	

	$('body').on('click', '.add-city-button', function(){
		/*
			TODO: valamiért ez az eseménykezelő le sem fut...
		*/
		
		var cityName       = $('#city-name').val();
		var cityPopulation = $('#city-population').val();
		var cityCountry    = $('#city-country').val();

		var cityData = {
			/*
				TODO: összeszedni az űrlap adatait
			*/
			name : cityName,
			population : cityPopulation,
			'country.iso' : cityCountry
		};
		$.ajax({
			url: '/rest/cities',
			method : 'PUT',
			type: "application/x-www-form-urlencoded",
			data : cityData,
			dataType : 'json',
			complete : function(){
				/*
					TODO: Be kellene csukni a popupot
				*/
				$('.close-modal').trigger('click');
			}
		});
	});

	
	/*
		TODO: ország törlés gombok működjenek
	*/
	$('table').on('click','.delete-country',function(){
		
		var countryIso = $(this).closest('tr').attr('data-iso');
		var countryIsoTD = $(this).closest('tr');
		if (confirm("Are you sure yo want to delete?")){
			$.ajax({
				url: '/rest/countries/'+ countryIso,
				method : 'DELETE',
				success : function(){
					$(countryIsoTD).remove();
				}
			});
		}
	});
	
	/*
		TODO: város törlés gombok működjenek
	*/

	$('table').on('click', '.delete-country', function(){
		
		var cityId = $(this).closest('tr').attr('data-id');
		var cityIdTD = $(this).closest('tr');
		if (confirm("Are you sure yo want to delete?")){
			$.ajax({
				url: '/rest/cities/' + cityId,
				method : 'DELETE',
				complete : function(){
					$(cityIdTD).remove();
				}
			});
		}
	});

});